import { applyMiddleware, createStore } from 'redux'
import logger from 'redux-logger';
import thunk from 'redux-thunk'
import { IState } from '../models';
import { rootReducer } from '../reducers';

export const configureStore = (initialState?: IState) => {
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(thunk, logger)
    );
}