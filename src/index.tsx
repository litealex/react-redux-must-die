import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { RootContainer } from './containers';
import registerServiceWorker from './registerServiceWorker';
import { configureStore } from './store'

import './styles.less';

ReactDOM.render(
  <Provider store={configureStore()} >
    <RootContainer />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
