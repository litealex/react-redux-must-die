
const createResponse = <T>(value: T) => {
    return new Promise((resolve) => {
        setTimeout(() => resolve(value), 100)
    });
}
const posts = [
    {
        id: 0,
        content: [
            'Обзор release notes',
            'Новый тип unknown',
            'Поддержка разных версий ts с typesVersions',
            'Переход с js на ts',
            'keyof',
            'Barrel pattern',
        ],
        link: 'http://podcast.hatejs.com/episodes/0.mp3',
        subtitle: 'Релиз typescript 3.0. Релиз typescript 3.1. Переход с js на ts. Barrel pattern.',
        title: 'Smart teapot: пилотный выпуск',
    },
    {
        id: 1,
        content: [
            'Обзор release notes',
            'Новый тип unknown',
            'Поддержка разных версий ts с typesVersions',
            'Переход с js на ts',
            'keyof',
            'Barrel pattern',
        ],
        link: 'http://podcast.hatejs.com/episodes/0.mp3',
        subtitle: 'Релиз typescript 3.0. Релиз typescript 3.1. Переход с js на ts. Barrel pattern.',
        title: 'Smart teapot: пилотный выпуск',
    }
]

export const podcastService = {
    getPodcast: (id: number) => {
        return createResponse(posts[id])
    },
    getPodcasts: () => {
        return createResponse(posts)
    }
};