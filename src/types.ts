export interface IActionComponentBase<T> {
    // tslint:disable-next-line:callable-types
    (value: T): void;
}