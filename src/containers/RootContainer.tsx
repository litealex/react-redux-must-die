import * as React from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Root } from '../components';
import { IState } from '../models'
import { PodcastContainer } from './PodcastContainer';
import { PodcastsContainer } from './PodcastsContainer'


const RootContainerFactory = (props: any) => {
    return (
        <Root>
            <Router>
                <div>
                    <Route path="/" exact={true} component={PodcastsContainer} />
                    <Route path="/podcast/:id" component={PodcastContainer} />
                </div>
            </Router>
        </Root>
    );
}

const mapStateToProps = (state: IState) => {
    return {

    }
};

export const RootContainer = connect(mapStateToProps)(RootContainerFactory);