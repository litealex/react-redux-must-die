import * as React from 'react'
import { connect } from 'react-redux'
import { deletePodcast, searchPodcasts } from '../actions/podcasts';
import {
    IPodcastsProps,
    Podcasts
} from '../components';
import { IState } from '../models';

export class PodcastsContainerFactory extends React.Component<IPodcastsProps> {
    public componentDidMount() {
        this.props.searchPosts();
    }
    public render() {
        return <Podcasts {...this.props} />
    }
}

const mapStateToProps = (state: IState): Partial<IPodcastsProps> => {
    return {
        items: state.podcasts
    }
};

const mapDispatchToProps = (dispath: any): Partial<IPodcastsProps> => {
    return {
        onDelete: (idx) => dispath(deletePodcast(idx)),
        searchPosts: () => dispath(searchPodcasts())
    }
}

export const PodcastsContainer =
    connect(mapStateToProps, mapDispatchToProps)
        (PodcastsContainerFactory);