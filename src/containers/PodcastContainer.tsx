import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { updateField } from '../actions/podcast'
import { EditPodcast, IEditPodcastProps } from '../components';
import { IFieldUpdatePayload, IState } from '../models';


class PodcastContainerFactory extends React.Component<IEditPodcastProps>{
    public render() {
        return <EditPodcast {...this.props} />;
    }
}

const mapStateToProps = (state: IState): Partial<IEditPodcastProps> => {
    return {
        item: state.podcast
    };
};

export const mapDispatchToProps = (dispatch: Dispatch): Partial<IEditPodcastProps> => {
    return {
        onFormChange: (p: IFieldUpdatePayload) => dispatch(updateField(p))
    };
}

export const PodcastContainer =
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(PodcastContainerFactory);





