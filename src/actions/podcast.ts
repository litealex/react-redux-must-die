// import { Dispatch } from 'redux';
// import { podcastService } from '../api'
import { ActionTypes } from '../constants'
import { createAction, IFieldUpdatePayload } from '../models';



export const updateField = (payload: IFieldUpdatePayload) => createAction(ActionTypes.PodcastFiledUpdate, payload);