import { Dispatch } from 'redux';
import { podcastService } from '../api'
import { ActionTypes } from '../constants'
import { createAction } from '../models';


export const deletePodcast = (idx: number) => createAction(idx, ActionTypes.DeletePodcast)


export const searchPodcasts = () => {
    return async (dispatch: Dispatch) => {
        try {
            dispatch(createAction(ActionTypes.LoadPodcast, await podcastService.getPodcasts()))
        } catch{ 
            dispatch(createAction(ActionTypes.LoadPodcastFail, null))
        }
    }
}