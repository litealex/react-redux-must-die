import { IState } from '../models';
import { podcastReducer } from './podcast'
import { podcastsReducer } from './podcasts'


export const rootReducer = (state: IState = {} as any, action: any): IState => {
    return {
        podcasts: podcastsReducer(state.podcasts, action),
        podcast: podcastReducer(state.podcast, action)
    };
};