import { ActionTypes } from '../constants';
import { IAction, IFieldUpdatePayload, IPodcast } from '../models';


const ininitialState: Partial<IPodcast> = {
    title: 'empty',
    descriptions: {
        items: [],
        title: ''
    }
};

const podcastFiledUpdateReducer = (state: IPodcast, action: IAction<IFieldUpdatePayload>) => {

    return {
        ...state,
        [action.payload.path]: action.payload.value
    }
};



export const podcastReducer = (state: IPodcast = ininitialState as any, action: IAction<any>): IPodcast => {
    switch (action.type) {
        case ActionTypes.PodcastFiledUpdate:
            return podcastFiledUpdateReducer(state, action);
    }
    return state;
}