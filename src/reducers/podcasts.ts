import { ActionTypes } from '../constants';
import { IPodcastShort } from '../models';
import { IAction } from '../models/action';
// import { combineReducers } from 'redux';


const deletePodcastReducer = (state: IPodcastShort[] = [], action: IAction<number>) => {
    return state.filter((x, idx) => idx !== action.payload);
};

const loadPodcastsReducer = (state: IPodcastShort[] = [], action: IAction<IPodcastShort[]>) => {
    return action.payload;
}

export const podcastsReducer = (state: IPodcastShort[] = [], action: IAction<any>): IPodcastShort[] => {
    switch (action.type) {
        case ActionTypes.DeletePodcast:
            return deletePodcastReducer(state, action);
        case ActionTypes.LoadPodcast:
            return loadPodcastsReducer(state, action);
    }
    return state;
};
