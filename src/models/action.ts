import { ActionTypes } from '../constants'


export interface IAction<T> {
    type: ActionTypes;
    payload: T
};

export interface IFieldUpdatePayload {
    path: string;
    value: any;
}

export const createAction = <T>(type: ActionTypes, payload: T): IAction<T> => ({
    payload,
    type
});