export type DateString = string;
export interface IPodcastShort {
    id: number;
    title: string;
    subtitle: string;
}

export interface IPodcast {
    id: number;
    title: string;
    subtitle: string;
    link: string;
    pubDate: DateString;
    enclosure: string;
    guid: string;
    duration: string;
    descriptions: {
        title: string,
        items: string[]
    }
}

export interface IState {
    podcasts: IPodcastShort[];
    podcast: IPodcast;
}