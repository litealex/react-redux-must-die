import * as React from 'react'
import { Link } from 'react-router-dom'

export interface IPodcastShortProps {
    id: number;
    title: string;
    subtitle: string;
}

export const PodcastShortItem = (props: IPodcastShortProps) => (
    <div className="st-podcast-short">
        <Link to={`/podcast/${props.id}`}>{props.title}</Link>
    </div>
);