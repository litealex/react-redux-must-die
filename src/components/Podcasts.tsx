import * as React from 'react'
import { IPodcastShort } from '../models'
import { PodcastShortItem } from './PodcastShortItem'

export interface IPodcastsProps {
    items: IPodcastShort[];
    onDelete: (idx: number) => void;
    searchPosts: () => void;

}
export class Podcasts extends React.Component<IPodcastsProps> {
    constructor(props: IPodcastsProps) {
        super(props)
        this.handleClick = this.handleClick.bind(this);
    }
    public handleClick() {
        this.props.onDelete(0)
    }
    public render() {
        const { items } = this.props;
        return (
            <div>
                <div>List of items</div>
                {
                    items.map((x) => <PodcastShortItem
                        id={x.id}
                        title={x.title}
                        subtitle={x.subtitle}
                        key={x.id} />)
                }
                <button onClick={this.handleClick}>Add new</button>
            </div>
        );
    }
}
