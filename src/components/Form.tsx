import * as React from 'react'

export interface IFormProps<T> {
    inner: boolean;
    item: T;
    childer: Element[]
}


export const FormContext = React.createContext({})

export class Form<T> extends React.Component<IFormProps<T>> {
    public static defaultProps: Partial<IFormProps<any>> = {
        inner: false
    }

    constructor(props: IFormProps<T>) {
        super(props);
    }
    public render() {
        const {
            children,
            inner,
            item
        } = this.props;
        const content = inner
            ? children
            : <form>{children}</form>
        return <FormContext.Provider value={item}>{content}</FormContext.Provider>
    }
}