import * as React from 'react'
import { IFieldUpdatePayload, IPodcast, } from '../models';
import { IActionComponentBase } from '../types'
import { Form } from './Form'
import { FormInput, IFormInputEvent } from './FormInput'
import { FormItem } from './FormItem'

export interface IEditPodcastProps {
    item: IPodcast;
    onFormChange: IActionComponentBase<IFieldUpdatePayload>;
}

export class EditPodcast extends React.Component<IEditPodcastProps> {
    constructor(props: IEditPodcastProps) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    public componentDidUpdate() {
        // this.setState(this.props.)
    }
    public render() {
        const item = this.props.item;
        return (
            <Form item={item}>
                <FormItem title='Title'>
                    <FormInput name='title' onChange={this.handleInputChange} />
                </FormItem>
                <FormItem title='Subtitle'>
                    <FormInput name='subtitle' onChange={this.handleInputChange} />
                </FormItem>
                <FormItem title='Link'>
                    <FormInput name='link' onChange={this.handleInputChange} />
                </FormItem>
                <FormItem title='Publication date'>
                    <FormInput name='pubDate' onChange={this.handleInputChange} />
                </FormItem>
                <FormItem title='Enclosure'>
                    <FormInput name='enclosure' onChange={this.handleInputChange} />
                </FormItem>
                <FormItem title='Guid'>
                    <FormInput name='guid' onChange={this.handleInputChange} />
                </FormItem>
                <FormItem title='Duration'>
                    <FormInput name='duration' onChange={this.handleInputChange} />
                </FormItem>
            </Form>);
    }

    private handleInputChange(e: IFormInputEvent) {
        const { name, value } = e;
        this.props.onFormChange({
            path: name,
            value
        })
    }
}