import * as React from 'react'
import { IActionComponentBase } from '../types'
import { FormContext } from './Form'


export interface IFormInputProp<T> {
    name: keyof T
    onChange: IActionComponentBase<IFormInputEvent>
}

export interface IFormInputEvent {
    name: string;
    value: string;
}

export class FormInput<T> extends React.Component<IFormInputProp<T>> {
    public static contextType = FormContext;
    constructor(props: IFormInputProp<T>, context: T) {
        super(props, context);
        this.handleChange = this.handleChange.bind(this);
    }

    public render() {
        const { name } = this.props;
        const item = this.context;
        const value = item[name] && item[name].toString() || '';
        return <input value={value} onChange={this.handleChange} />;
    }
    private handleChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.props.onChange({
            value: e.target.value,
            name: this.props.name.toString()
        });
    }
}