import * as React from 'react'
export interface IFormItemProps {
    title: string;
    children?: any;

}
export const FormItem = ({ title, children }: IFormItemProps) => {
    return (
    <div className='st-form-item'>
        <div className='st-form-item__title'>{title}</div>
        <div className="st-form-item__content">
            {children}
        </div>
    </div>)
};