import * as React from 'react'

export class Root extends React.Component {
    public render() {
        return (
            <div>
                {this.props.children}                
            </div>
        )
    }
}