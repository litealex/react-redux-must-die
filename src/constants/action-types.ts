export enum ActionTypes {
    DeletePodcast = 1,
    LoadPodcast,
    LoadPodcastFail,
    PodcastFiledUpdate
}